﻿Param(
    [parameter(Mandatory=$false)]
    [alias("t")]
    $BuildType
)
cls
$Program1C = "C:\Program Files (x86)\1cv81\bin\1cv8.exe";
$configs = @{}
$configs['project1'] = @{
    "projectname" = "project_name";
    "buildpath" = "c:\builds\current\";
    "ibfile" = 1;
    "server" = "";
    "ib" = "c:\ib\project_name\";
    "iblogin" = "builder";
    "ibpassword" = "123";
    "rep"    = "c:\1c-repositories\project_name\";
    "replogin" = "builder";
    "reppassword" = "123";
    "deploydebug" = "\\ftpServer\project_name\test";
    "deployrelis" = "\\ftpServer\project_name\pubs";
}

function ComProperty ([System.__ComObject]$obj, [string]$value)
{
	$return_value = [System.__ComObject].invokemember($value,[System.Reflection.BindingFlags]::GetProperty,$null,$obj, $null)	
	return $return_value;
}

function ComMethod ([System.__ComObject]$obj, [string]$Method, [Array]$Params)
{
	$return_value = [System.__ComObject].invokemember($Method,[System.Reflection.BindingFlags]::invokeMethod,$null,$obj, $Params)	
	return $return_value;
}


foreach($config in $configs.Values){
    echo "Build project " $config.projectname

    cd $config.buildpath
    $conectionstring = '";Usr="' + $config.iblogin + '";Pwd="' + $config.ibpassword + '";'
    $configauth = "CONFIG "
    $repauth = " /ConfigurationRepositoryF" + $config.rep`
        + ' /ConfigurationRepositoryN"' + $config.replogin + '"'`
        + ' /ConfigurationRepositoryP"' + $config.reppassword + '"'
    if ($config.ibfile) {
        $conectionstring = 'File="' + $config.ib + $conectionstring
        $configauth  = $configauth  + " /F" + $config.ib
    } else {
        $conectionstring = 'Srvr="' + $config.server + '";Ref="' + $config.ib + $conectionstring
        $configauth  = $configauth  + " /S" + $config.server + "\" + $config.ib
    }
    $configauth = $configauth`
        + ' /N"' + $config.iblogin + '"'`
        + ' /P"' + $config.ibpassword + '"'
    
    
    $repupdate = $configauth + $repauth + " /ConfigurationRepositoryUpdateCfg -force /UpdateDBCfg"
    
    $chekbase = $configauth + $repauth`
        + " /CheckConfig -Client -ClientServer -ExternalConnectionServer -ExternalConnection -Server -DistributiveModules -ConfigLogicalIntegrity /DumpResult error.txt"
    
    echo "Update from repository..."
    start-process $Program1C $repupdate -Wait
    echo "ok."

    echo "Check config..."
    start-process $Program1C $chekbase -Wait
    
    $Connector1c = new-object -comobject "V81.COMConnector"
    $Base = $Connector1c.Connect($conectionstring)
    $medata = ComProperty $Base "Metadata"
    [String]$buildversion = ComProperty $medata "Version"
    [System.Runtime.Interopservices.Marshal]::ReleaseComObject($Base)
    
    if (!$buildversion.CompareTo("")) {
        echo "Failed get version. Build stoped."
        continue
    }
    
    If (Test-Path $buildversion) {
        echo "Build $buildversion is exist. Buld stoped" 
        Continue
    }
     
    "Build $buildversion..."
    
    if (!(Test-Path error.txt)) {
        echo "Failed check config. Buaild stoped."
        continue
    }
    
    if ((Get-Content error.txt | Select-Object -Last 1).CompareTo("0")) {
        echo "Incorrect config. Build stoped."
        Continue
    }
    
    [String]$preversions = ""
    ls * -Include 1cv8.cf -Recurse | split-path -parent | split-path -leaf | foreach {$preversions = $preversions + " -f $_\1cv8.cf -v $_ "}
    
    $buildcommand = $configauth + $repauth + " /CreateDistributionFiles -cffile $buildversion\1cv8.cf -cfufile $buildversion\1cv8.cfu" + $preversions
    start-process $Program1C $buildcommand -Wait
    echo "ok."
    if ($BuildType -eq "relis") {
        $target = $config.deployrelis;
    } else {
        $target = $config.deploydebug;
    }
    
    echo "deploying to $target..."
    Copy-Item $buildversion $target -recurse
    echo "ok"    
}